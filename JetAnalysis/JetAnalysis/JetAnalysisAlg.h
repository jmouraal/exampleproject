#ifndef JETANALYSIS_JETANALYSISALG_H
#define JETANALYSIS_JETANALYSISALG_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include <xAODJet/JetContainer.h>
#include <xAODEgamma/ElectronContainer.h>

class JetAnalysisAlg : public AthAlgorithm{
    public:
         JetAnalysisAlg (const std::string &name, ISvcLocator* p5vcLocator);
         virtual ~JetAnalysisAlg();

         virtual StatusCode initialize() override;
         virtual StatusCode execute() override;
         virtual StatusCode finalize() override;
    
    private:
         Gaudi:: Property<float> m_electronPtCut {this, "ElectronPtCut", 3.3,"Offiline Electron Pt Cut"};
};
#endif
