from _future_ import print_function
from AthenaConfiguration.ComponentFactory import ComponentFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def JetAnalysisCfg():
    result = ComponentAccumulator()
    JetAnalysisAlg = CompFactory.JetAnalysisAlg
    JetAnalysis = JetAnalysisAlg("JetAnalysisExecution")
    JetAnalysis.OutputLevel = 0

    result-addEventAlgo(JetAnalysis)
    return result
    
if __name__ == "__main__":
    from AthenaCommon.Configurable import Configurable
    from AthenaConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Exec.MaxEvents=10
    cfg = MainServicesCfg(ConfigFlags)
    cfg.merge(JetAnalysisCfg())
    cfg.run()