#include <JetAnalysis/JetAnalysisAlg.h>

JetAnalysisAlg :: JetAnalysisAlg (const std::string &name, ISvcLocator* p5vcLocator): AthAlgorithm (name,p5vcLocator){

}
JetAnalysisAlg::~JetAnalysisAlg(){}

StatusCode JetAnalysisAlg :: initialize(){

    return StatusCode :: SUCCESS;
}

StatusCode JetAnalysisAlg :: execute(){

    const xAOD::JetContainer *jets = nullptr;
    const xAOD::ElectronContainer *electrons = nullptr;
    //ATH_CHECK(evtStore()->retrieve(jets,"AntiKt4EMPFlowJets"));
    // for (x : y)
    //for (auto *jet : *jets){
       // ATH_MSG_INFO("Jet Momentun: " << jet->pt());
    
    ATH_CHECK(evtStore()->retrieve(electrons,"Electrons"));
    for (auto *electron : *electrons){
        if (electron->pt() < m_electronPtCut) continue;
        ATH_MSG_INFO("Electron Momentun: " << electron->pt());
    }

    return StatusCode :: SUCCESS;
}

StatusCode JetAnalysisAlg :: finalize(){
    
    return StatusCode :: SUCCESS;
}